import React, {Component} from 'react';
import Video from './Components/Video';
import Sidebar from './Components/Sidebar';
import { Provider } from 'react-redux'
import store from './store'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <Video/>
          <Sidebar/>
        </Provider>
      </div>
    );
  }
}

export default App;
